Source: apkinspector
Section: python
Priority: optional
Maintainer: Dale Richards <dale@dalerichards.net>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-sphinx,
 python3-all,
 python3-poetry-core,
 pybuild-plugin-pyproject,
Standards-Version: 4.6.2
Homepage: https://github.com/erev0s/apkInspector
Vcs-Browser: https://salsa.debian.org/doge-tech/apkinspector
Vcs-Git: https://salsa.debian.org/doge-tech/apkinspector.git

Package: python3-apkinspector
Architecture: all
Multi-Arch: foreign
Depends:
 ${python3:Depends},
 ${misc:Depends},
Provides: apkinspector
Description: provide insights into the structure of Android APK files
 apkInspector is a tool designed to provide detailed insights into the zip
 structure of APK files, offering the capability to extract content and decode
 the AndroidManifest.xml file. What sets APKInspector apart is its adherence
 to the zip specification during APK parsing, eliminating the need for
 reliance on external libraries. This independence allows APKInspector to be
 highly adaptable, effectively emulating Android's installation process for
 APKs that cannot be parsed using standard libraries. The main goal is to
 enable users to conduct static analysis on APKs that employ evasion
 techniques, especially when conventional methods prove ineffective.
 .
 This package provides the apkInspector command-line tool and the resquired
 Python3 libraries.
